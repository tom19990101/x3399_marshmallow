/*
 * Copyright (C) 2015 Andreas Schneider <asn@cryptomilk.org>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "memtrack_ion"
#define LOG_NDEBUG 1 

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>

#include <cutils/log.h>
#include <hardware/memtrack.h>

#include "memtrack.h"
#include <dirent.h>

#define ARRAY_SIZE_ION(x) (sizeof(x)/sizeof(x[0]))
#ifndef MIN_ION
#define MIN_ION(x, y) ((x) < (y) ? (x) : (y))
#endif

const char *ion_client = "/sys/kernel/debug/ion/clients";


struct memtrack_record record_templates_ion[] = {
	{       .flags = MEMTRACK_FLAG_SMAPS_ACCOUNTED |
                 MEMTRACK_FLAG_PRIVATE |
                 MEMTRACK_FLAG_NONSECURE,
    },
    {
        .flags = MEMTRACK_FLAG_SMAPS_UNACCOUNTED |
                 MEMTRACK_FLAG_PRIVATE |
                 MEMTRACK_FLAG_NONSECURE,
    },
};
int check_process_ion_count(pid_t pid)
{
	int count=0;
    DIR *dirp; 
	const char *fmt_pid = "%d-";
	char ion_file[64] = { 0 };
	snprintf(ion_file, sizeof(ion_file), fmt_pid,pid);
	int len=strlen(ion_file);
    struct dirent *dp;
    dirp = opendir(ion_client); 
    while ((dp = readdir(dirp)) != NULL) { 
        
		if(strncmp (dp->d_name,ion_file,len)==0)
		{
			//ALOGI("%s", dp->d_name );
			count++;
		}
    }      
    (void) closedir(dirp);
    return count;
}

int ion_memtrack_get_memory(pid_t pid,
                            int type,
                            struct memtrack_record *records,
                            size_t *num_records)
{
	ALOGV("ion(%d) type=%d :*num_records=%zd",pid,type,*num_records);
	size_t allocated_records = MIN_ION(*num_records, ARRAY_SIZE_ION(record_templates_ion));
	
    struct stat sb;
    //const char *ion_path = "/sys/kernel/debug/ion/clients";
    const char *fmt = "%s/%d-%d";
    char ion_file[128] = { 0 };
    FILE *fp;
    int cmp;
	int sum=0;
    int rc;
    char line[120];
	int count=0;
	
	*num_records = ARRAY_SIZE_ION(record_templates_ion);
    /* fastpath to return the necessary number of records */
    if (allocated_records == 0) {
		//ALOGV("allocated_records =0 ,so return 0");
        return 0;
    }
	    memcpy(records, record_templates_ion,
           sizeof(struct memtrack_record) * allocated_records);



    if (*num_records == 0) {
		//ALOGD("num_records is invalid when for:%s",ion_file);
        return -EINVAL;
    }
	count=check_process_ion_count(pid);
	if(count==0)
	{
		ALOGV("count=0 for:%d",pid);
		return -EINVAL;
	}
	int i=0;
	for(i=0;i<count;i++)
	{
	    snprintf(ion_file, sizeof(ion_file), fmt, ion_client, pid,i);

	    ALOGV("Open ion file: %s", ion_file);

	    fp = fopen(ion_file, "r");
	    if (fp == NULL) {
		    //ALOGE("num_records is invalid when for:%s",ion_file);
	        return -errno;
	    }

	    //*num_records = 1;

	    for(;;) {
			char label[40] = { 0 };
	        unsigned int x;

	        if (fgets(line, sizeof(line), fp) == NULL) {
	            break;
	        }
	        /* Format:
				 heap_name:    size_in_bytes
			   system-heap: 		32276480
				  cma-heap: 		16384000
	         	*/
			
	        rc = sscanf(line, "%s %u",label,&x);
	        if (rc == 2) {
				if(x>0)
					ALOGV("get %s ion size (in bytes): %u",label, x);
				if(strncmp(label, "system-heap", 11)==0 || strncmp(label, "cma-heap", 8)==0)
					sum+=x;
				
	        } 
	    }
	    fclose(fp);
	}
    records[0].flags = MEMTRACK_FLAG_SMAPS_UNACCOUNTED|MEMTRACK_FLAG_PRIVATE|MEMTRACK_FLAG_NONSECURE;
    records[0].size_in_bytes = sum;
	//ALOGW("-get %d ion size (in bytes): %d",pid, sum);


    return 0;
}
